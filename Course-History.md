## 课程介绍

《金融大数据与量化分析》，英文：Financial Big Data and Quantitative Analysis， 简写：FBDQA. 

2018年春季，《金融大数据与量化分析》正式开课。 

目前，课程定位为“科学课组类”通识课程（建设中）

## 课程价值：

了解计算机量化投资的基本概念，学习计算机编程与量化技术基础。

掌握数据处理的基本原理和工具，学习掌握量化交易的基本原理、量化交易策略设计与量化交易系统实现等。

## Table of Contents

1. Finance Market and Investment Management.
2. Principle of computing with Python. 
3. Finacial Data System operating and modeling with Python.
4. Principle of quantative trading technology.
5. Course Projects.

## 现任助教及历任助教

----


2023-2024-春： 冀泽华

----

2022-2023-秋： 冀泽华

2022-2023-春： 冀泽华

----

2022-2023-秋： 冀泽华

2021-2022-春： 倪运升、严涛

----

2021-2022-秋：邓理睿、申栋垚、倪运升

2020-2021-春：梁宸、邓理睿

----

2020-2021-秋：刘为为、申栋垚、倪运升

2019-2020-春：刘为为、池映天

----

2019-2020-秋：刘为为、池映天、倪运升

2018-2019-春：刘为为、

----

2018-2019-秋：刘为为

2017-2018-春：刘为为、郑文勋、常嘉辉

----

## 以往课程Git项目链接
-- https://gitee.com/saturnlab/FBDQA-2023A

-- https://gitee.com/saturnlab/FBDQA-2023S

-- https://gitee.com/saturnlab/FBDQA-2022A

-- https://gitee.com/saturnlab/FBDQA-2022S

-- https://github.com/saturn-lab/FBDQA-2021A

-- https://github.com/saturn-lab/FBDQA-2021S

-- https://github.com/saturn-lab/FBDQA-2020A

-- https://github.com/saturn-lab/FBDQA-2020S