### 课程小结：金融大数据与量化交易 - 第三讲

#### 1. ETF基础概念与特点
- **ETF类型**：包括股票ETF（宽基、窄基）、债券ETF、黄金ETF、货币ETF等。
- **交易特性**：
  - 可在一级市场申购或赎回，亦可在二级市场买卖。
  - 交易成本低，无印花税和过户费，手续费低。
  - 大部分ETF支持T+0交易，例外为股票ETF实施T+1。
  
#### 2. ETF折溢价套利原理
- **折价ETF套利**：场内买入低于净值的ETF，赎回成分股后场外卖出。
- **溢价ETF套利**：场外用股票申购ETF，场内以高于净值的价格卖出ETF。

#### 3. 重要ETF与市值分类
- **市值分类**：如SPY/IV/VOO（标普500），VTI（全美股），QQQ（纳斯达克100）等。
- **风格分类**：如大盘成长型（VUG）、大盘价值型（VTV）。
- **策略因子ETF**：如MTUM（动量因子），IWD（价值因子）。

#### 4. CAPM与APT经典理论
- **量化交易观念**：追求可控的风险收益比，利用大数定律，分散投资，确保策略在概率上可重复。

#### 5. 因子研究与历史回测
- **美股因子**：市值、价值、动量、波动、质量等。
- **历史回测方式**：每年重新平衡，通过做多表现好的股票组，做空表现差的股票组。

#### 6. 多因子策略框架
- **因子构建流程**：数据获取、因子测试、因子预处理、多因子模型回测。
- **因子库搭建**：选择高频交易类因子与低频财务因子，确保因子的多样性和覆盖性。

#### 7. 因子测试与评估
- **单因子测试**：利用IC值和RankIC值衡量因子的有效性和稳定性。
- **数据与因子预处理**：使用中位数极值法修正异常值，行业市值中性化处理等。
