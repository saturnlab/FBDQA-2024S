## 第十讲课程小结 - 期权进阶

### 1. 期权基本策略
- **四大基本交易策略**：
  - **Buy Call**（预期大涨）
  - **Sell Call**（预期不大涨）
  - **Buy Put**（预期大跌）
  - **Sell Put**（预期不大跌）
- **机构与散户行为**：
  - 机构倾向于作为卖方，因为主要收入来源于期权的时间价值（类似于卖废纸）。
  - 散户通常作为买方，更倾向于赌大涨或大跌。

### 2. 策略选择与应用
- **牛市价差策略**：
  - 适用于对标的长期看涨但短期可能波动或回调的情况。
  - 结合Buy Call与Sell Call进行操作，尽管胜率较低但可规避大幅回撤，长期获益于上涨。
- **熊市价差策略**：
  - 买入行权价高的Put，卖出行权价低的Put。
  - 选择平值或轻度实值的Put，同时卖出轻度虚值的Put。

### 3. 期权希腊字母解析
- **Delta**（Δ）：衡量标的资产价格变动对期权价值的影响。
- **Gamma**（Γ）：衡量标的资产价格变动时，Delta的变化率。
- **Vega**（ν）：标的资产的波动率变动对期权价值的影响。
- **Theta**（θ）：时间推移对期权价值的影响。
- **Rho**（ρ）：无风险利率变动对期权价值的影响。

### 4. 风险管理
- **买方风险**：
  - 风险有限（最大亏损为权利金），但亏损频繁。
  - 理论上，仓位控制（轻仓操作）是主要的风险控制方法。
- **卖方风险**：
  - 理论上风险无限，需要防范黑天鹅事件。
  - 关键是避开市场大事件，合理选择合约和控制仓位。

### 5. 期权交易决策流程
- **行情分析**：确定市场趋势（看涨、看跌、震荡等）。
- **波动率判断**：决定采用的策略类型。
- **策略与合约选择**：
  - 单腿、多腿、合成、期现组合。
  - 考虑行权价、到期月份、数量和比例。
- **风控与动态调整**：
  - 资金分配、使用杠杆、设定止盈/止损、根据希腊字母动态调仓。

### 6. 实际操作案例
- **案例1：备兑开仓**
  - 同时买入标的证券与卖出认购期权。
  - 常用于市场缓涨或缓跌情况，利用权利金降低持仓成本。
- **案例4：Sell Call（卖废纸）**
  - 在波动率高时卖出Call，利用波动率的均值回归特性获利。