##### 自我介绍

- 姓名：戴诗琪
- 性别：女
- 院系/班级：建筑学院建01 
- 兴趣爱好：唱歌+听歌，滑冰，弹琴

![微信图片_20230227134502](C:\Users\lenovo\Desktop\常用文件\微信图片_20230227134502.jpg)

本学期课程信息表

| 课程名称             | 上课地点           | 上课时间 | 课程学分 |
| -------------------- | ------------------ | -------- | -------- |
| 金融大数据与量化分析 | 李兆基科技大楼B570 | 1-12周   | 3        |
| 交通大数据分析与建模 | 清华学堂214        | 全周     | 3        |
| 综合论文训练         | 有方大厦A 1502     | 1-12周   | 15       |

