# FBDQA-2024S
Financial Big Data and Quantitative Analytics, Spring 2024.

# 金融大数据与量化分析 

Financial Big Data and Quantitative Analytics, a.k.a FBDQA.

课程编号[150-10313]()

## 教学计划

[教学计划安排-2024S](Schedule/FBDQA-Schedule-2024S.md)

## 教师与助教

Instructors: [Hao Wang](https://gitee.com/w13901168931), [Zhen Chen](http://www.icenter.tsinghua.edu.cn/info/1060/1298.htm) , Yisong Zhang, Xiaodong Ma, Min Guo, Haoyu Wang, Yin Gao

TAs:  Ze-hua Ji

## 课程历史

《金融大数据与量化分析》自2018年春季设立以来，已开设12次。

详细的发展过程见[课程历史](Course-History.md)

## 课程内容

1. 基础部分：数字化能力基础，Python编程语言、SQL数据管理语言... 

2. 单元内容 [量化交易](Schedule/Quant-Schedule.md) ：量化交易体系、信号指标、评价体系，期权、聚宽平台SDK操作等

3. 单元内容 [金融大数据](Schedule/FBD-Schedule.md) ：数据分析、机器学习、金融模型的数学工具等

## 目录结构

- Computing / 计算基础与python学习
- Course-Projects/ 课程实践任务
- Schedule / 课程每周安排
- Study-Memo / 学习小结目录
- learnFBD /  金融大数据
- learnQuant / 量化交易
- Logistics  / 课程管理相关

## Notice:

This course is targeted to general education in a sense, and appropriate for students with programming skills with one language and more.

I would be very grateful if you post your feedback as an issue or pull request in this GitHub.

