# 教学日历-2024春季 

|  校历周 |   日期    | 内容                                        | 关键词、概述 | 讲课 / 实践 |         作业 （公告发布，非网络学堂）       |         作业（网络学堂验收批改）    |
| ------ | --------- | ------------------------------------------- | ------------ | ----------- | ------------------- | ------- |
| 第0周  | 2024-02-19 |    [2024春季-金融大数据量化分析-课前准备](../Schedule/WW1-9/WW0/WW0-Plan.md)      |   建立微信群    |   讲课  |  发布  |    验收     |
| 第1周  | 2024-02-26 | [2024春季-金融大数据量化分析-第一课-教学计划](../Schedule/WW1-9/WW1/WW1-Plan.md)    |     | 讲课   |   |        |
| 第2周  | 2024-03-04 | [2024春季-金融大数据量化分析-第二课-教学计划](../Schedule/WW1-9/WW2/WW2-Plan.md)    |     | 讲课   |         |         |
| 第3周  | 2024-03-11 | [2024春季-金融大数据量化分析-第三课-教学计划](../Schedule/WW1-9/WW3/WW3-Plan.md)    |     | 讲课   |  [课程作业2-因子研究分组实践作业预发布](../Course-Project/Project-2-midterm.md)     | 随堂小测讲解   |
| 第4周  | 2024-03-18 | [2024春季-金融大数据量化分析-第四课-教学计划](../Schedule/WW1-9/WW4/WW4-Plan.md)    |     | 讲课  |          |  |
| 第5周  | 2024-03-25 | [2024春季-金融大数据量化分析-第五课-教学计划](../Schedule/WW1-9/WW5/WW5-Plan.md)    |     | 机构走访 |   |  |
| 第6周  | 2024-04-01 | [2024春季-金融大数据量化分析-第六课-教学计划](../Schedule/WW1-9/WW6/WW6-Plan.md)    |    | 讲课   | [课程作业1-金融大数据CAPM作业](../Course-Project/Project-3-capm.md)、  [课程作业1-Kelly公式](../Course-Project/Project-1-Kelly.md) |  |
| 第7周  | 2024-04-08 | [2024春季-金融大数据量化分析-第七课-教学计划](../Schedule/WW1-9/WW7/WW7-Plan.md)    |     | 讲课 | [课程作业2-因子研究分组实践作业发布](../Course-Project/Project-2-midterm.md)    |    |
| 第8周  | 2024-04-15 | [2024春季-金融大数据量化分析-第八课-教学计划](../Schedule/WW1-9/WW8/WW8-Plan.md)    |      | 讲课   |  |  课程作业2-验收    |
| 第9周  | 2024-04-22 | [2024春季-金融大数据量化分析-第九课-教学计划](../Schedule/WW1-9/WW9/WW9-Plan.md)    |      | 讲课   |   |   课程作业1-验收      |
| 第10周 | 2024-04-29 | [2024春季-金融大数据量化分析-第十课-教学计划](../Schedule/WW10-12/WW10/WW10-Plan.md)  |      | 讲课   |     |   |
| 第11周 | 2024-05-06 | [2024春季-金融大数据量化分析-第十一课-教学计划](../Schedule/WW10-12/WW11/WW11-Plan.md) |     |    |  [课程大作业-量化竞赛活动发布](../Course-Project/Project-4-All.md)  课程大作业-量化模型预提交    |  |
| 第12周 | 2024-05-13 | [2024春季-金融大数据量化分析-第十二课-教学计划](../Schedule/WW10-12/WW12/WW12-Plan.md) |     | 结课 |     |            |
| 第13周 | 2024-05-20 | [2024春季-金融大数据量化分析-第十三课-教学计划](../Schedule/WW13-15/WW13/WW13-Plan.md)     |     |  线上辅导 |     |         |
| 第14周 | 2024-05-27 | [2024春季-金融大数据量化分析-第十四课-教学计划](../Schedule/WW13-15/WW14/WW14-Plan.md)     |     |  线下辅导 |     |         |
| 第15周 | 2024-06-03 | [2024春季-金融大数据量化分析-第十五课-教学计划](../Schedule/WW13-15/WW15/WW15-Plan.md)  |   期末报告  | 讲课 （结课） |   期末大作业-报告 | End结束      |



# 平时作业及期末大作业
|  校历周 |   日期    | 作业内容                                        | 关键词 | 最终提交截止日期 |    负责助教老师       |
| ------ | --------- | ------------------------------------------- | ------------ | ----------- | :------------: |
| 第8周  | 2024-04-15 |   [课程平时作业2-因子研究分组实践作业](../Course-Project/Project-2-midterm.md)      |   分组路演    |  2024-04-10  | 冀助教  |
| 第9周  | 2024-04-22 | [课程平时作业1-金融大数据CAPM作业](../Course-Project/Project-3-capm.md)    |  见网络学堂作业发布   | 2024-04-25 |   郭老师     |
| 第15周  | 2024-06-03 | [课程期末大作业-竞赛即替代性作业](../Course-Project/Project-4-All.md)     |  评测平台   | 2024-06-31  | 邓博士   |
