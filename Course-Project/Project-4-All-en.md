# [Final Project] Quantitative Strategy Invitational and Model Prediction Challenge

Assessing the overall learning and mastery of the course.

1. Divided into two categories:
   * Classic Quantitative Strategy Group: Individuals develop strategies and present report presentations.
   * Model Prediction Group: Recommended for individuals, but can also form teams with a maximum of 2 members.
2. Alternative: Write a comprehensive report on "The Application of Artificial Intelligence in Quantitative Trading."

Schedule:

| Schedule | Date (University Calendar) |
| -------- | ---- |
| Deadline for Registration (applicable to both groups) | Weeks 8-11 |
| Release of Training Data for Model Prediction Group | Week 8 (preferably earlier) |
| Opening of Training Environment | Week 9 (preferably earlier) |
| Presentation Date | Week 15 (both groups on the same day) |

## Classic Quantitative Strategy Group

* Trading variety unrestricted.
* Strategy type unrestricted.
* Strongly recommend completing a single-factor test or a test of a multi-factor model, without focusing on strategy structure (such as position, risk control, etc.).
* For practicality, initial capital is uniformly set to 10 million RMB. If only conducting a factor test, initial capital can be set to infinite, using returns for statistical analysis.
* The report must clarify the trading logic, with complete backtesting results and analysis.
* Backtesting trading records must be included in the report attachments.
* Report format is flexible, it can be a detailed document or just a presentation ppt.
* Voluntary submission of strategy code.

Evaluation Criteria:
* Format Requirements (20%)
  * Completeness of strategy logic description
  * Inclusion of detailed backtesting results and analysis explanation
* Strategy Logic Integrity (30%)
  * Inclusion of key elements of the strategy (underlying assets, entry, exit, position, risk control, trading costs, etc.)
  * Inclusion of key trading record analysis explanation
* Rigor in Strategy Implementation (30%)
  * Use of realistic trading costs as backtesting parameters
  * Absence of lookahead bias or overfitting phenomena
  * Whether the backtesting time span can support the evaluation of strategy effectiveness
* Usability and Innovation of the Strategy (20%)
  * Whether the strategy has practical value
  * Whether the strategy logic is novel and consistent with market characteristics

## Model Prediction Group

* Training and validation on specified datasets.
* Selection based on performance on a common test set.

Data Preparation:
* Normalization of L1 data for 10 stocks (snapshot: Limit Order Book, Last Price, Volume), establishing an offline database.
* Release of Training Set (including validation set).
* Public Test Set (not released).

Selection Rules:
* Model training and evaluation on offline data.
* Prediction target: movement of the Last Price.
* Time span: 5, 10, 20, 40, 60 ticks.
  * Last Price Movement Categories:
    * Upward
    * Downward
    * Unchanged.
* Model Evaluation Metrics:
  * Prediction Accuracy: max{a5, a10, a20, a40, a60}, where a5 represents the accuracy of price movement prediction after 5 ticks, and so on.
  * P&L: Using the time scale with the highest prediction accuracy as the benchmark, construct simulated trading backtesting for the smallest trading unit, ignoring trading costs, and calculate the P&L obtained.
  * Reasonableness, novelty, completeness of the solution.

## Alternative Solution Requirements

* Write a comprehensive report on "The Application of Artificial Intelligence in Quantitative Trading."
* Completeness of the format (20%)
  * Includes standardized citations of materials.
  * Contains summaries of material content and discussions of core viewpoints.
* Comprehensive material (40%)
  * Includes the latest research literature in this field domestically and internationally.
  * Bonus points: Reference to no less than 10 papers.
  * Bonus points: Includes the number of papers published in top AI conferences in the last 5 years.
* Clarity of the review (40%)
  * Includes summaries of referenced literature and summaries of core arguments.
  * Includes personal understanding of literature and viewpoints.
* Report Submission Date: Week 15.